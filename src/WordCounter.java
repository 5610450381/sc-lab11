import java.util.HashMap;


public class WordCounter {
	private String mes;
	private HashMap<String, Integer> count;

	public WordCounter(String mes) {
		// TODO Auto-generated constructor stub
		this.mes = mes;
	}
	public void count(){
		
		
		count = new HashMap<String, Integer>();
		String[] strings = mes.split(" ");
		int val = 1;
		for (String string : strings){
			if(count.containsKey(string)){
			val = count.get(string);
			count.remove(string);
			count.put(string, val+1);

		}
		else{
			count.put(string, 1);
		}
	}
}

	public int hasWord(String word){
		if(count.containsKey(word) == true){
			int val = count.get(word);
			return val;
		}
		return 0;
	}
}